(* This functor is applied to a type of terms, equipped with visitor classes.
   It produces a toolbox of useful functions that operate on terms. *)

module Make (Term : ToolboxInput.INPUT) = struct

  open Term

(* -------------------------------------------------------------------------- *)

  (* A raw term is one where every name is represented as a string. This form
     is typically produced by a parser, and consumed by a printer. It is not
     used internally. *)

  type raw_term =
    (string, string) term

  (* A nominal term is one where every name is represented as an atom. Although
     this is not visible in this type definition, we may additionally impose a
     Global Uniqueness Hypothesis (GUH), that is, we may require every binding
     name occurrence to carry a distinct atom. *)

  type nominal_term =
    (Atom.t, Atom.t) term

(* -------------------------------------------------------------------------- *)

  (* All of the code is produced by macro-expansion. *)

  (* This serves as a test of our macros, which can also be used directly by
     the end user, in situations where the functor [Toolbox.Make] cannot be
     used. *)

  #include "AlphaLibMacros.cppo.ml"

  __FA
  FA(term)

  __FILTER
  FILTER(term)

  __BA
  BA(term)

  __AVOIDS
  AVOIDS(term)

  __GUQ
  GUQ(term)

  __COPY
  COPY(term)

  __AVOID
  AVOID(term)

  __SHOW
  SHOW(term)

  __IMPORT
  IMPORT(term)

  __EXPORT
  EXPORT(term)

  __SIZE
  SIZE(term)

  __EQUIV
  EQUIV(term)

  (* Mnemonic: Substitute for variables in terms. *)
  __SUBST(TVar)
  SUBST(TVar, term)

(* -------------------------------------------------------------------------- *)

end
