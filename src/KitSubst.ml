(* This kit serves to construct a [subst] function for terms -- a function
   that substitutes things (possibly terms) for atoms. *)

(* An environment is a map of atoms to things. We require every binder [x]
   encountered along the way to be fresh with respect to [env]. *)

type 'thing env =
  'thing Atom.Map.t

let extend x env =
  (* We would like to check that [x] is fresh for [env], but can only
     perform the domain check. The codomain check cannot be performed
     since the type of things is abstract here. *)
  assert (not (Atom.Map.mem x env));
  (* Since [x] is fresh for [env], no capture is possible. Thus, no
     freshening of the bound name is required. Thus, we can keep the
     substitution [env], unchanged, under the binder. *)
  x, env

class ['self] map = object (_ : 'self)
  method private extend = extend
  (* [visit_'fn] is not implemented, as it is up to the user to identify
     variable nodes and replace them. This requires overriding a suitable
     visitor method. *)
  method private visit_'fn _env _x = assert false
end

let apply (copy : 'thing -> 'thing)
          (env : 'thing env)
          (this : 'thing) (x : Atom.t)
: 'thing =
  match Atom.Map.find x env with
  | u ->
      (* Possibly copy the term that is grafted, so as to maintain global
         uniqueness. The client controls which [copy] operation should be
         used here. *)
      copy u
  | exception Not_found ->
      (* [x] is not affected by the substitution, so the original thing is
         returned. [this] should be [Var x], where [Var] is the constructor
         for variables in the syntax of things. *)
      this
