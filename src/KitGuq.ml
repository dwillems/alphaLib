(* This kit serves to compute the set of ``bound atoms'' of a term (as in
   [KitBa]) and at the same time, we check that the term satisfies ``global
   uniqueness'', that is, no atom is bound twice inside this term (not even
   along distinct branches). The exception [Atom.Set.NonDisjointUnion x] is
   raised if the atom [x] occurs twice in a binding position. *)

class ['self] reduce = object (_ : 'self)

  method private extend _x () =
    ()

  method private visit_'fn () _x =
    Atom.Set.empty

  (* The monoid of sets of atoms, equipped with disjoint union, is used. *)
  (* This can cause a [NonDisjointUnion] exception to be raised. *)
  inherit [_] Atom.Set.disjoint_union_monoid

  (* An atom is added to the set of bound atoms when its scope is exited. *)
  method private restrict x xs =
    if Atom.Set.mem x xs then
      raise (Atom.Set.NonDisjointUnion x)
    else
      Atom.Set.add x xs

end
