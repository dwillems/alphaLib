(* This kit serves to compute the set of ``bound atoms'' of a term, that is,
   the set of all binding name occurrences. *)

class ['self] reduce = object (_ : 'self)

  method private extend _x () =
    ()

  method private visit_'fn () _x =
    Atom.Set.empty

  (* The monoid of sets of atoms is used. *)
  inherit [_] Atom.Set.union_monoid

  (* An atom is added to the set of bound atoms when its scope is exited. *)
  method private restrict x xs =
    Atom.Set.add x xs

end
