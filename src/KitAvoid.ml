(* This kit serves to construct an [avoid] function for terms. This
   function renames the bound names of a term, if necessary, so as
   to avoid a certain set of bad names. *)

type env =
  KitCopy.env

let empty =
  KitCopy.empty

let lookup =
  KitCopy.lookup

let extend bad x env =
  (* If [x] is bad, it must be renamed. Otherwise, keep it. *)
  if Atom.Set.mem x bad then
    KitCopy.extend x env
  else
    x, env

class ['self] map bad = object (_ : 'self)
  method private extend x env = extend bad x env
  method private visit_'fn env x = lookup env x
end
