type void

(* -------------------------------------------------------------------------- *)

(* A universal, concrete type of single-name abstractions. *)

(* We wish to represent all kinds of abstractions -- e.g. in nominal style,
   in de Bruijn style, etc. -- so we parameterize the abstraction over the
   type ['bn] of the bound name and over the type ['term] of the body. This
   makes this type definition almost trivial -- it is just a pair -- but it
   still serves as a syntactic marker of where abstractions are located. *)

type ('bn, 'term) abs =
  'bn * 'term

(* -------------------------------------------------------------------------- *)

(* A definition is a binding form of the form [let (rec) x = t in u]. The name
   [x] is in scope in [u]. It is in scope in [t] if the definition is marked
   recursive; otherwise, it is not. The terms [t] and [u] need not belong to the
   same syntactic category. *)

type recursive =
  | NonRecursive
  | Recursive

type ('bn, 't, 'u) def =
  recursive * 'bn * 't * 'u

let choose recursive env env' =
  match recursive with
  | NonRecursive ->
      env
  | Recursive ->
      env'

(* -------------------------------------------------------------------------- *)

(* The main effect of an abstraction is to cause the environment to be
   enriched when the abstraction is traversed. The following classes define
   where the environment is enriched. *)

(* These classes do not know the type of the environment, and do not know how
   it is enriched; the latter task is delegated to virtual methods, such as
   [extend] and [restrict]. The implementation of these methods is provided
   by separate ``kits''. *)

(* We need one class per variety of visitor, which is a bit painful. *)

(* The method [visit_abs] is polymorphic in the type of terms. This is
   important, as it means that one can use several instances of [abs] in a
   single type definition and still be able to construct well-typed
   visitors. *)

(* The virtual methods [extend] and [restrict] are not polymorphic in the
   types of bound names and environments. On the contrary, each kit comes
   with certain specific types of bound names and environments. *)

(* Because [iter] and [iter2] are special cases of [reduce] and [reduce2],
   respectively, we define them that way, so as to save effort. *)

(* -------------------------------------------------------------------------- *)

(* [map] *)

class virtual ['self] map = object (self : 'self)

  method private visit_'bn: void -> void -> void
  = fun _ _ -> assert false

  method private virtual extend: 'bn1 -> 'env -> 'bn2 * 'env

  method private visit_abs: 'term1 'term2 .
    _ ->
    ('env -> 'term1 -> 'term2) ->
    'env -> ('bn1, 'term1) abs -> ('bn2, 'term2) abs
  = fun _ f env (x1, t1) ->
      let x2, env' = self#extend x1 env in
      x2, f env' t1

  method private visit_def: 't1 't2 'u1 'u2 .
    _ ->
    ('env -> 't1 -> 't2) ->
    ('env -> 'u1 -> 'u2) ->
    'env -> ('bn1, 't1, 'u1) def -> ('bn2, 't2, 'u2) def
  = fun _ f g env (recursive, x1, t1, u1) ->
      let x2, env' = self#extend x1 env in
      let t2 = f (choose recursive env env') t1 in
      let u2 = g env' u1 in
      recursive, x2, t2, u2

end

(* -------------------------------------------------------------------------- *)

(* [endo] *)

class virtual ['self] endo = object (self : 'self)

  method private visit_'bn: void -> void -> void
  = fun _ _ -> assert false

  method private virtual extend: 'bn -> 'env -> 'bn * 'env

  method private visit_abs: 'term .
    _ ->
    ('env -> 'term -> 'term) ->
    'env -> ('bn, 'term) abs -> ('bn, 'term) abs
  = fun _ f env ((x1, t1) as this) ->
      let x2, env' = self#extend x1 env in
      let t2 = f env' t1 in
      if x1 == x2 && t1 == t2 then
        this
      else
        x2, t2

  method private visit_def: 't 'u .
    _ ->
    ('env -> 't -> 't) ->
    ('env -> 'u -> 'u) ->
    'env -> ('bn, 't, 'u) def -> ('bn, 't, 'u) def
  = fun _ f g env ((recursive, x1, t1, u1) as this) ->
      let x2, env' = self#extend x1 env in
      let t2 = f (choose recursive env env') t1 in
      let u2 = g env' u1 in
      if x1 == x2 && t1 == t2 && u1 == u2 then
        this
      else
        recursive, x2, t2, u2

end

(* -------------------------------------------------------------------------- *)

(* [reduce] *)

class virtual ['self] reduce = object (self : 'self)

  method private virtual plus: 'z -> 'z -> 'z

  method private visit_'bn: void -> void -> void
  = fun _ _ -> assert false

  method private virtual extend: 'bn -> 'env -> 'env

  method private virtual restrict: 'bn -> 'z -> 'z

  method private visit_abs: 'term .
    _ ->
    ('env -> 'term -> 'z) ->
    'env -> ('bn, 'term) abs -> 'z
  = fun _ f env (x, t) ->
      let env' = self#extend x env in
      self#restrict x (f env' t)

  method private visit_def: 't 'u .
    _ ->
    ('env -> 't -> 'z) ->
    ('env -> 'u -> 'z) ->
    'env -> ('bn, 't, 'u) def -> 'z
  = fun _ f g env (recursive, x, t, u) ->
      let env' = self#extend x env in
      let zt = f (choose recursive env env') t in
      let zu = self#restrict x (g env' u) in
      self#plus zt zu

end

(* -------------------------------------------------------------------------- *)

(* [iter] *)

class virtual ['self] iter = object (_ : 'self)

  inherit ['self] reduce

  method private plus () () = ()
  method private restrict _ () = ()

end

(* -------------------------------------------------------------------------- *)

(* [map2] *)

class virtual ['self] map2 = object (self : 'self)

  method private visit_'bn: void -> void -> void -> void
  = fun _ _ _ -> assert false

  method private virtual extend: 'bn1 -> 'bn2 -> 'env -> 'bn3 * 'env

  method private visit_abs: 'term1 'term2 'term3 .
    _ ->
    ('env -> 'term1 -> 'term2 -> 'term3) ->
    'env -> ('bn1, 'term1) abs -> ('bn2, 'term2) abs -> ('bn3, 'term3) abs
  = fun _ f env (x1, t1) (x2, t2) ->
      let x3, env' = self#extend x1 x2 env in
      x3, f env' t1 t2

  method private visit_def: 't1 'u1 't2 'u2 't3 'u3 .
    _ ->
    ('env -> 't1 -> 't2 -> 't3) ->
    ('env -> 'u1 -> 'u2 -> 'u3) ->
    'env -> ('bn1, 't1, 'u1) def -> ('bn2, 't2, 'u2) def -> ('bn3, 't3, 'u3) def
  = fun _ f g env (recursive1, x1, t1, u1) (recursive2, x2, t2, u2) ->
      if recursive1 <> recursive2 then VisitorsRuntime.fail();
      let x3, env' = self#extend x1 x2 env in
      let t3 = f (choose recursive1 env env') t1 t2 in
      let u3 = g env' u1 u2 in
      recursive1, x3, t3, u3

end

(* -------------------------------------------------------------------------- *)

(* [reduce2] *)

class virtual ['self] reduce2 = object (self : 'self)

  method private virtual plus: 'z -> 'z -> 'z

  method private visit_'bn: void -> void -> void -> void
  = fun _ _ _ -> assert false

  method private virtual extend: 'bn1 -> 'bn2 -> 'env -> 'env

  method private virtual restrict: 'bn1 -> 'bn2 -> 'z -> 'z

  method private visit_abs: 'term1 'term2 .
    _ ->
    ('env -> 'term1 -> 'term2 -> 'z) ->
    'env -> ('bn1, 'term1) abs -> ('bn2, 'term2) abs -> 'z
  = fun _ f env (x1, t1) (x2, t2) ->
      let env' = self#extend x1 x2 env in
      self#restrict x1 x2 (f env' t1 t2)

  method private visit_def: 't1 'u1 't2 'u2 .
    _ ->
    ('env -> 't1 -> 't2 -> 'z) ->
    ('env -> 'u1 -> 'u2 -> 'z) ->
    'env -> ('bn1, 't1, 'u1) def -> ('bn2, 't2, 'u2) def -> 'z
  = fun _ f g env (recursive1, x1, t1, u1) (recursive2, x2, t2, u2) ->
      if recursive1 <> recursive2 then VisitorsRuntime.fail();
      let env' = self#extend x1 x2 env in
      let zt = f (choose recursive1 env env') t1 t2 in
      let zu = self#restrict x1 x2 (g env' u1 u2) in
      self#plus zt zu

end

(* -------------------------------------------------------------------------- *)

(* [iter2] *)

class virtual ['self] iter2 = object (_ : 'self)

  inherit ['self] reduce2

  method private plus () () = ()
  method private restrict _ _ () = ()

end
