open AlphaLib
open F

(* -------------------------------------------------------------------------- *)

(* Type environments. *)

module TermVar =
  String

module TermVarMap =
  Map.Make(TermVar)

type env = {
  tevars: nominal_typ TermVarMap.t;
  tyvars: Atom.Set.t
}

let empty : env =
  { tevars = TermVarMap.empty; tyvars = Atom.Set.empty }

exception UnboundTermVariable of tevar

let lookup (env : env) (x : tevar) : nominal_typ =
  try
    (* The free type variables of this type cannot be captured; see below. *)
    TermVarMap.find x env.tevars
  with Not_found ->
    raise (UnboundTermVariable x)

let extend_with_tevar (env : env) (x : tevar) (ty : nominal_typ) : env =
  (* We maintain the invariant that the free type variables in the codomain
     of [env.tevars] form a subset of [env.tyvars]. *)
  assert (Atom.Set.subset (fa_typ ty) env.tyvars);
  { env with tevars = TermVarMap.add x ty env.tevars }

let extend_with_tyvar (env : env) (a : tyvar) : env =
  (* We assume that type variables are globally unique in the term that we are
     type-checking. Thus, the \Lambda-bound name [a] cannot be in the domain
     of the environment (i.e., it cannot have been previously bound). Therefore,
     by the above invariant, it cannot be in the codomain of the environment
     either. This implies that it is safe to look up a type in the environment. *)
  assert (not (Atom.Set.mem a env.tyvars));
  { env with tyvars = Atom.Set.add a env.tyvars }

(* -------------------------------------------------------------------------- *)

(* Destructors. *)

exception NotAnArrow of raw_typ

let as_arrow xenv ty : nominal_typ * nominal_typ =
  match ty with
  | TyArrow (ty1, ty2) ->
      ty1, ty2
  | _ ->
      raise (NotAnArrow (export_typ xenv ty))

exception NotAProduct of raw_typ

let as_product xenv ty : nominal_typ * nominal_typ =
  match ty with
  | TyProduct (ty1, ty2) ->
      ty1, ty2
  | _ ->
      raise (NotAProduct (export_typ xenv ty))

exception NotAForall of raw_typ

let as_forall xenv ty : tyvar * nominal_typ =
  match ty with
  | TyForall (a, ty) ->
      a, ty
  | _ ->
      raise (NotAForall (export_typ xenv ty))

(* -------------------------------------------------------------------------- *)

(* An equality test. *)

exception TypeMismatch of raw_typ * raw_typ

let equate xenv ty1 ty2 =
  if not (equiv_typ ty1 ty2) then
    raise (TypeMismatch (export_typ xenv ty1, export_typ xenv ty2))

(* -------------------------------------------------------------------------- *)

(* The type-checker. *)

(* Precondition: the term [t] that we started with satisfies global uniqueness.
   So, the term [t] that we are looking satisfies global uniqueness and avoids
   the set of in-scope type variables, [env.tyvars]. *)

(* Precondition: if [env] maps [x] to some type [ty], then the bound variables
   of [ty] are disjoint with [env.tyvars] and with the bound variables of [t],
   and [ty] has no shadowing. *)

(* Postcondition: if [typeof env t] returns a type [ty], then the bound variables
   of [ty] are disjoint with [env.tyvars], and [ty] has no shadowing. *)

(* As we go down, we build an export environment [xenv], which is used when a
   type error is encountered. The problematic types are then exported, so that
   a type error message can be printed. This export environment is enriched
   when we enter a \Lambda. *)

let rec typeof xenv env (t : nominal_term) : nominal_typ =
  (* Test the preconditions stated above. *)
  assert (guq_term t);
  assert (avoids_term env.tyvars t);
  assert (
    let bad = Atom.Set.union env.tyvars (ba_term t) in
    TermVarMap.for_all (fun _x ty ->
      avoids_typ bad ty
    ) env.tevars
  );
  let ty = match t with
  | TeVar x ->
      lookup env x
  | TeAbs (x, ty1, t) ->
      let ty2 = typeof xenv (extend_with_tevar env x ty1) t in
      TyArrow (ty1, ty2)
  | TeApp (t, u) ->
      let ty1, ty2 = as_arrow xenv (typeof xenv env t) in
      equate xenv (typeof xenv env u) ty1;
      ty2
  | TeLet (x, t, u) ->
      let env = extend_with_tevar env x (typeof xenv env t) in
      typeof xenv env u
  | TeTyAbs (a, t) ->
      let _, xenv = KitExport.extend a xenv in
      TyForall (a, typeof xenv (extend_with_tyvar env a) t)
  | TeTyApp (t, ty2) ->
      let a, ty1 = as_forall xenv (typeof xenv env t) in
      (* We need ba(ty1) # [ty2/a] for this substitution to be safe. *)
      (* We have ba(ty1) # a because the type a.ty1 satisfies no-shadowing. *)
      assert (not (Atom.Set.mem a (ba_typ ty1)));
      (* We have ba(ty1) # fa(ty2) because fa(ty2) is a subset of dom(env), that is,
         env.tyvars, and the result of typeof avoids env.tyvars. *)
      (* We must copy when grafting so as to ensure that the result of the
         substitution enjoys no-shadowing. *)
      subst_TyVar_typ1 copy_typ ty2 a ty1
      (* Conversely, I believe that we could choose to copy [ty1] and not copy
         [ty2] when grafting: *)
      (* subst_TyVar_typ1 (fun ty2 -> ty2) ty2 a (copy_typ ty1) *)
  | TePair (t1, t2) ->
      TyProduct (typeof xenv env t1, typeof xenv env t2)
  | TeProj (i, t) ->
      assert (i = 1 || i = 2);
      let ty1, ty2 = as_product xenv (typeof xenv env t) in
      if i = 1 then ty1 else ty2
  in
  (* Test the postcondition stated above. *)
  assert (avoids_typ env.tyvars ty);
  ty

let typeof =
  typeof KitExport.empty empty
