open AlphaLib
open BindingForms

(* In this demo, only type variables are handled via AlphaLib. Term variables
   are represented as strings. *)

(* Type variables. *)

type tyvar =
  Atom.t

(* Types. *)

#define TYP ('fn, 'bn) typ

type TYP =
  | TyVar of 'fn
  | TyArrow of TYP * TYP
  | TyProduct of TYP * TYP
  | TyForall of ('bn, TYP) abs

(* Term variables. *)

and tevar = (string[@opaque])

(* Terms. *)

#define TERM ('fn, 'bn) term

and TERM =
  | TeVar of tevar
  | TeAbs of tevar * TYP * TERM
  | TeApp of TERM * TERM
  | TeLet of tevar * TERM * TERM
  | TeTyAbs of ('bn, TERM) abs
  | TeTyApp of TERM * TYP
  | TePair of TERM * TERM
  | TeProj of int * TERM

(* Visitor generation. *)

[@@deriving
  visitors { variety = "iter";   ancestors = ["BindingForms.iter"] },
  visitors { variety = "map";    ancestors = ["BindingForms.map"]  },
  visitors { variety = "endo";   ancestors = ["BindingForms.endo"] },
  visitors { variety = "reduce"; ancestors = ["BindingForms.reduce"] },
  visitors { variety = "iter2";  ancestors = ["BindingForms.iter2"] } ]

(* Type abbreviations. *)

type raw_typ =
  (string, string) typ
type nominal_typ =
  (Atom.t, Atom.t) typ

type raw_term =
  (string, string) term
type nominal_term =
  (Atom.t, Atom.t) term

(* Operations based on visitors. *)

(* We create more operations than we actually need in this demo.
   This is just a way of testing that everything works. *)

#include "AlphaLibMacros.cppo.ml"

__FA
FA(typ)
FA(term)

__FILTER
FILTER(typ)
FILTER(term)

__BA
BA(typ)
BA(term)

__AVOIDS
AVOIDS(typ)
AVOIDS(term)

__GUQ
GUQ(typ)
GUQ(term)

__COPY
COPY(typ)
COPY(term)

__AVOID
AVOID(typ)
AVOID(term)

__SHOW
SHOW(typ)
SHOW(term)

__IMPORT
IMPORT(typ)
IMPORT(term)

__EXPORT
EXPORT(typ)
EXPORT(term)

__SIZE
SIZE(typ)
SIZE(term)

__EQUIV
EQUIV(typ)
EQUIV(term)

__SUBST(TyVar)
SUBST(TyVar, typ)
SUBST(TyVar, term)
