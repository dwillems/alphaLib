open PPrint
open PPrintAux
open F

let arrow =
  string "->"

let doublebackslash =
  string "\\\\"

let forall =
  string "forall"

let rec typ0 ty =
  match ty with
  | TyVar x ->
      string x
  | TyArrow _
  | TyProduct _
  | TyForall _ ->
     parens (typ ty)

and typ1 ty =
  match ty with
  | TyProduct (ty1, ty2) ->
      group (typ0 ty1 ^/^ star ^/^ typ1 ty2)
  | _ ->
      typ0 ty

and typ2 ty =
  match ty with
  | TyArrow (ty1, ty2) ->
      group (typ1 ty1 ^/^ arrow ^/^ typ2 ty2)
  | TyForall (x, ty) ->
      group (forall ^/^ string x ^^ dot ^/^ typ2 ty)
  | _ ->
      typ1 ty

and typ ty =
  typ2 ty

let rec term0 t =
  match t with
  | TeVar x ->
      string x
  | TeAbs _
  | TeApp _
  | TeLet _
  | TeTyAbs _
  | TeTyApp _
  | TePair _
  | TeProj _ ->
      parens (term t)

and term1 t =
  match t with
  | TeApp (t1, t2) ->
      app (term1 t1) (term0 t2)
  | TeTyApp (t1, ty2) ->
      app (term1 t1) (brackets (typ ty2))
  | TeProj (i, t) ->
      app (string (Printf.sprintf "proj%d" i)) (term0 t)
  | _ ->
      term0 t

and term2 t =
  match t with
  | TePair (t1, t2) ->
      group (term1 t1 ^^ comma ^/^ term2 t2)
  | _ ->
      term1 t

and term3 t =
  match t with
  | TeAbs (x, ty, t) ->
      block
        (backslash ^^ string x ^^ spacecolon)
        (break 1 ^^ typ ty)
        (break 1 ^^ dot)
      ^/^
      term3 t
  | TeLet (x, t1, t2) ->
      block
        (string "let" ^/^ string x ^/^ equals ^^ space)
        (term t1)
        (string " in")
      ^/^
      term t2
  | TeTyAbs (a, t) ->
      group (
        doublebackslash ^^ string a ^^ space ^^ dot
        ^/^
        term3 t
      )
  | _ ->
      term2 t

and term t =
  term3 t

let typ =
  adapt typ

let term =
  adapt term
