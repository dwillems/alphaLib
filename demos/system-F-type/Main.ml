open Printf
open AlphaLib
open F
open FTypeChecker

(* -------------------------------------------------------------------------- *)

(* Typechecking a term. *)

let check (t : raw_term) =
  printf "Raw term:\n%a\n"
    Print.term t
  ;
  let t : nominal_term =
    import_term KitImport.empty t
  in
  printf "Imported (and reexported) term:\n%a\n"
    Print.term (export_term KitExport.empty t)
  ;
  match typeof t with
  | ty ->
      printf "Inferred type:\n%a\n"
        Print.typ (export_typ KitExport.empty ty)
  | exception NotAnArrow ty ->
      printf "Type error: this is not a function type:\n%a\n"
        Print.typ ty
  | exception NotAProduct ty ->
      printf "Type error: this is not a product type:\n%a\n"
        Print.typ ty
  | exception NotAForall ty ->
      printf "Type error: this is not a universal type:\n%a\n"
        Print.typ ty

(* -------------------------------------------------------------------------- *)

(* Sample well-typed terms. *)

let identity =
  TeTyAbs ("A", TeAbs ("x", TyVar "A", TeVar "x"))

let terms : raw_term list = [

  identity;

  TeTyAbs ("A", TeAbs ("x", TyVar "A",
    TeApp (TeTyApp (identity, TyVar "A"), TeVar "x")
  ));

  TeTyAbs ("A", TeAbs ("x", TyVar "A",
  TeTyAbs ("B", TeAbs ("y", TyVar "B",
    TeLet ("id", identity,
      TePair (
        TeApp (TeTyApp (TeVar "id", TyVar "A"), TeVar "x"),
        TeApp (TeTyApp (TeVar "id", TyVar "B"), TeVar "y")
      )
    )
  ))));

]

let () =
  List.iter check terms

(* -------------------------------------------------------------------------- *)

(* Sample ill-typed terms. *)

let ill_typed : raw_term list = [

  TeTyAbs ("A", TeAbs ("x", TyVar "A", TeApp (TeVar "x", TeVar "x")));

  TeProj (1, identity);

  TeTyAbs ("A",
    TeLet ("id", TeAbs ("x", TyVar "A", TeVar "x"),
      TeTyApp (TeVar "id", TyVar "A")
    )
  );

]

let () =
  List.iter check ill_typed

(* TEMPORARY report unbound term variables, with locations. *)
(* TEMPORARY report unbound type variables, with locations. *)
