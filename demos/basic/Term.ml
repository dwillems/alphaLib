open AlphaLib
open BindingForms

type ('fn, 'bn) term =
  | TVar of 'fn
  | TLambda of ('bn, ('fn, 'bn) term) abs
  | TApp of ('fn, 'bn) term * ('fn, 'bn) term

  [@@deriving

    visitors { variety = "iter"; public = ["visit_term"];
               ancestors = ["BindingForms.iter"] }
    ,
    visitors { variety = "map"; public = ["visit_term"];
               ancestors = ["BindingForms.map"] }
    ,
    visitors { variety = "endo"; public = ["visit_term"];
               ancestors = ["BindingForms.endo"] }
    ,
    visitors { variety = "reduce"; public = ["visit_term"];
               ancestors = ["BindingForms.reduce"] }
    ,
    visitors { variety = "iter2"; public = ["visit_term"];
               ancestors = ["BindingForms.iter2"] }

  ]
