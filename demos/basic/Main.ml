open Printf
open AlphaLib
open Term
open TermGenerator
open ParallelRed

module T =
  Toolbox.Make(Term)

open T

let ( ** ) = Atom.Set.union

let subst1 =
  subst_TVar_term1 copy_term

(* -------------------------------------------------------------------------- *)

(* [interval i j] constructs a list representation of the semi-open interval
   [i..j). *)

let rec interval i j : int list =
  if i < j then
    i :: interval (i + 1) j
  else
    []

(* [init i j f] constructs a list of the values [f i] up to [f (j - 1)]. *)

let init i j (f : int -> 'a) : 'a list =
  List.map f (interval i j)

(* [take n xs] returns the first [n] elements of [xs]. *)

let rec take n xs =
  match n, xs with
  | 0, _
  | _, [] ->
      []
  | _, (x :: xs as input) ->
     let xs' = take (n - 1) xs in
     if xs == xs' then
       input
     else
       x :: xs'

(* [split xs] splits the set of atoms [xs] in two halves. *)

let halve xs =
  let xs = Atom.Set.elements xs in
  let n = List.length xs in
  let xs = take (n/2) xs in
  Atom.Set.of_list xs

(* -------------------------------------------------------------------------- *)

(* A non-hygienic printer of arbitrary terms. This printer shows the internal
   identity of atoms, using [Atom.show]. *)

let nhprint oc t =
  Print.term oc (show_term t)

(* A hygienic printer of closed terms. This printer uses [export]. *)

let hprint oc t =
  Print.term oc (export_term KitExport.empty t)

(* -------------------------------------------------------------------------- *)

(* A test run. *)

module Run (P : sig

  (* Test parameters. *)

  val number: int
  val size: int
  val atoms: int

end) = struct open P

let () =
  printf "Test run: number = %d, size = %d, atoms = %d.\n%!"
    number size atoms

(* A collection of closed raw terms. *)

let closed_raw_terms : raw_term list =
  printf "Generating random closed raw terms...\n%!";
  init 0 number (fun _i ->
    generate_raw size
  )

(* Import them, so as to obtain a collection of closed nominal terms. *)

let closed_nominal_terms : nominal_term list =
  printf "Importing these raw terms...\n%!";
  List.map (import_term KitImport.empty) closed_raw_terms

let on_closed_nominal_terms f =
  List.iter f closed_nominal_terms

(* The closed terms should be closed and well-formed. *)

let () =
  printf "Checking closedness and well-formedness...\n%!";
  on_closed_nominal_terms (fun t ->
    assert (closed_term t);
    assert (Atom.Set.is_empty (fa_term t));
    assert (guq_term t)
  )

(* A collection of random (non-closed, non-well-formed) nominal terms. *)

let arbitrary_nominal_terms : nominal_term list =
  printf "Generating random nominal terms...\n%!";
  init 0 number (fun _i ->
    generate_nominal atoms size
  )

let on_arbitrary_nominal_terms f =
  List.iter f arbitrary_nominal_terms

(* Copy them, so as to obtain guq (although non-closed) nominal terms. *)

let guq_nominal_terms : nominal_term list =
  printf "Copying these terms...\n%!";
  List.map copy_term arbitrary_nominal_terms

let on_guq_nominal_terms f =
  List.iter f guq_nominal_terms

let all_atoms_of_guq_nominal_terms =
  List.fold_left (fun atoms t ->
    atoms ** ba_term t ** fa_term t
  ) Atom.Set.empty guq_nominal_terms


(* These terms should be well-formed. *)

let () =
  printf "Checking well-formedness...\n%!";
  on_guq_nominal_terms (fun t ->
    assert (guq_term t)
  )

(* [size_term] should succeed. *)

let () =
  printf "Testing size...\n%!";
  on_arbitrary_nominal_terms (fun t ->
    ignore (size_term t : int)
  )

(* [show_term] should succeed. *)

let () =
  printf "Testing show...\n%!";
  on_arbitrary_nominal_terms (fun t ->
    ignore (show_term t : raw_term)
  )

(* On a closed term, [show] is actually hygienic. *)

let () =
  printf "Testing show on closed terms...\n%!";
  on_closed_nominal_terms (fun t ->
    let u : raw_term = show_term t in
    let v : nominal_term = import_term KitImport.empty u in
    assert (equiv_term t v)
  )

(* Round-trip property of [import] and [export] on closed terms. *)

let () =
  printf "Testing import/export round-trip...\n%!";
  on_closed_nominal_terms (fun t ->
    let u : raw_term = export_term KitExport.empty t in
    let v : nominal_term = import_term KitImport.empty u in
    assert (equiv_term t v)
  )

(* [fa] and [occurs] should be consistent with each other. *)

let () =
  printf "Comparing fa and closed...\n%!";
  on_arbitrary_nominal_terms (fun t ->
    assert (Atom.Set.is_empty (fa_term t) = closed_term t)
  );
  on_guq_nominal_terms (fun t ->
    assert (Atom.Set.is_empty (fa_term t) = closed_term t)
  )

(* Our well-formed terms should be disjoint. *)

let () =
  printf "Checking disjointness...\n%!";
  assert (guq_terms closed_nominal_terms);
  assert (guq_terms guq_nominal_terms)

(* [copy] should be the identity, up to alpha-equivalence. *)

let test_copy t =
  assert (equiv_term t (copy_term t))

let () =
  printf "Testing equiv/copy...\n%!";
  on_arbitrary_nominal_terms test_copy;
  on_guq_nominal_terms test_copy;
  on_closed_nominal_terms test_copy

(* Test substitution. *)

let size_of_u = 100

let () =
  printf "Testing substitution...\n%!";
  let (=) = equiv_term in
  on_guq_nominal_terms (fun t ->
    (* TEMPORARY should choose randomly and efficiently *)
    match Atom.Set.choose (fa_term t) with
    | exception Not_found ->
        ()
    | x ->
        let u = generate_nominal atoms size_of_u in
        let t' = subst1 u x t in
        assert (guq_term t');
        assert (Atom.Set.equal
                  (fa_term t')
                  (fa_term u ** Atom.Set.remove x (fa_term t))
               );
        assert (t' = subst1 u x t);
        assert (guq_term (TLambda (x, t)));
        (* subst1 u x (TLambda (x, t)) = TLambda (x, t) *)
        (* cannot be checked as these are illegal arguments to substitution *)
        begin match t with
        | TVar _ -> assert false
        | TLambda (y, t) ->
            assert (subst1 u x (TLambda (y, t)) =
                    TLambda (y, subst1 u x t))
        | TApp (t1, t2) ->
            assert (subst1 u x (TApp (t1, t2)) =
                    TApp (subst1 u x t1, subst1 u x t2))
        end
  );
  let x = Atom.freshh "x"
  and y = Atom.freshh "y" in
  let u = generate_nominal atoms size_of_u in
  assert (subst1 u x (TVar x) = u);
  assert (subst1 u x (TVar y) = TVar y);
  on_guq_nominal_terms (fun t ->
    assert (subst1 u x t = t);
    assert (subst1 u x t == t) (* note physical equality *)
  );
  (* Test that [equiv] distinguishes certain terms. *)
  assert (not (TVar x = TVar y));
  assert (not (TLambda (x, TVar x) = TLambda (y, TVar x)));
  assert (not (TVar x = TLambda (x, TVar x)));
  assert (not (TLambda (x, TLambda (y, TVar x)) = TLambda (x, TLambda (y, TVar y))));
  (* Negative test of [avoids] and [guq]. *)
  assert (not (avoids_term (Atom.Set.singleton x) (TLambda (x, TVar y))));
  assert (not (avoids_term (Atom.Set.empty) (TLambda (x, TLambda (x, TVar y)))));
  assert (not (guq_term (TApp (TLambda (x, TVar y), TLambda (x, TVar y)))));
  ()

(* [fa] and [ba] should be consistent with each other. *)

let () =
  printf "Checking fa and ba are consistent...\n%!";
  on_guq_nominal_terms (fun t ->
    let ba = ba_term t
    and fa = fa_term t in
    assert (Atom.Set.disjoint ba fa);
    assert (avoids_term fa t)
  )

(* Test [avoid] and [avoids]. Must use guq terms because [avoids] requires
   no shadowing. *)

let () =
  printf "Computing all atoms of our guq terms...\n%!";
  let some_atoms = halve (halve (all_atoms_of_guq_nominal_terms)) in
  printf "Testing avoids on %d of %d atoms...\n%!"
    (Atom.Set.cardinal some_atoms)
    (Atom.Set.cardinal all_atoms_of_guq_nominal_terms)
  ;
  on_guq_nominal_terms (fun t ->
    let t' = avoid_term some_atoms t in
    assert (equiv_term t t');
    assert (avoids_term some_atoms t')
  )

(* [fa] and [occurs] should be consistent with each other. *)

let () =
  printf "Comparing fa and occurs... (quadratic)\n%!";
  on_arbitrary_nominal_terms (fun t ->
    let fa = fa_term t in
    Atom.Set.iter (fun a ->
      assert (occurs_term a t); (* slow *)
    ) fa
  );
  on_guq_nominal_terms (fun t ->
    let ba = ba_term t in
    let fa = fa_term t in
    assert (Atom.Set.is_empty (Atom.Set.inter ba fa));
    Atom.Set.iter (fun a ->
      assert (not (occurs_term a t)) (* slow *)
    ) ba
  )

end

(* -------------------------------------------------------------------------- *)

(* Run tests with increasing parameter sizes. *)

let rec run number size atoms =
  if size <= 3000 then
    let module R = Run(struct
      let number = number
      let size = size
      let atoms = atoms
    end) in
    run number (3 * size) (2 * atoms)

let () =
  run 1000 1 1

(* Sample terms. *)

let x =
  Atom.freshh "x"

let y =
  Atom.freshh "y"

let id =
  TLambda (x, TVar x)

let delta_body =
  TApp (TVar x, TVar x)

let delta =
  TLambda (x, delta_body)

let omega =
  TApp (delta, copy_term delta)

let samples = [
    TVar y;
    id;
    TApp (id, TVar y);
    TApp (id, TVar x);
    TApp (id, copy_term id);
    delta;
    omega;
    import_term KitImport.empty (TLambda ("x", TVar "x"));
    import_term KitImport.empty (TLambda ("z", TLambda ("z", TVar "z")));
  ]

let closed_samples = [
    id;
    TApp (id, copy_term id);
    delta;
    omega;
    import_term KitImport.empty (TLambda ("z", TLambda ("z", TVar "z")));
  ]

let evaluate f =
  List.iter f samples

let () =
  printf "Testing size...\n";
  [
    TVar y, 1;
    id, 2;
    TApp (id, TVar y), 4;
    TApp (id, copy_term id), 5;
    delta, 4;
    omega, 9;
  ] |> List.iter (fun (t, i) ->
    assert (size_term t = i)
  )

let print_copy_term t =
  printf "copy_term(%a) = %a\n%!"
    nhprint t
    nhprint (copy_term t)

let () =
  evaluate print_copy_term

let print_export t =
  printf "export(%a) = %a\n%!"
    nhprint t
    hprint t

let () =
  List.iter print_export closed_samples

let print_fa t =
  printf "fa(%a) = %a\n%!"
    nhprint t
    Atom.Set.print (fa_term t)

let () =
  evaluate print_fa

let print_subst1 u x t =
  let t' = subst1 u x t in
  printf "substituting %a for %a in %a = ...\n  %a\n%s\n%!"
    nhprint u
    Atom.print x
    nhprint t
    nhprint t'
    (if t == t' then "(physically equal)" else "(physically distinct)")

let () =
  print_subst1 (TVar y) x (TVar x);
  print_subst1 (TVar y) x (TVar y);
  print_subst1 delta x delta_body;
  print_subst1 (copy_term delta) x (copy_term delta);
  ()

let print_equiv t1 t2 =
  printf "equiv: %a ~ %a = %b\n%!"
    nhprint t1
    nhprint t2
    (equiv_term t1 t2)

let () =
  print_equiv id id;
  print_equiv id (TVar x);
  print_equiv (TVar x) (TVar y);
  print_equiv delta (copy_term delta);
  print_equiv omega (copy_term omega);
  print_equiv (TLambda (x, TVar x)) (TLambda (y, TVar y));
  print_equiv (TLambda (x, TVar x)) (TLambda (y, TVar x));
  print_equiv
    (import_term KitImport.empty (TLambda ("z", TLambda ("z", TVar "z"))))
    (import_term KitImport.empty (TLambda ("z", TLambda ("y", TVar "z"))))
  ;
  ()

let print_guq t =
  printf "guq(%a) = %b\n%!"
    nhprint t
    (guq_term t)

let () =
  evaluate print_guq;
  print_guq (TApp (id, id))

(*
let () =
  for _i = 0 to 9 do
    let t = import_term KitImport.empty (generate_raw 15) in
    printf "generate_raw() = %a\n%!" hprint t;
    assert (closed_term t);
    assert (Atom.Set.is_empty (fa_term t));
  done

let () =
  for _i = 0 to 9 do
    let t = generate_nominal (* atoms: *) 5 (* size: *) 15 in
    printf "generate_nominal() = %a\n%!" nhprint t
  done
 *)
