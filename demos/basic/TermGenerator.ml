(* TEMPORARY only 26 possible strings in [random_name]; fix that  *)
(* TEMPORARY alter the balance of lambda versus app in [generate] *)
(* TEMPORARY possibly implement shrinking *)

open Term

module StringSet =
  OST.Make(String)

module AtomSet =
  OST.Make(AlphaLib.Atom)

(* Randomly split an integer [n] into [n1 + n2]. *)

let split n =
  assert (n >= 0);
  let n1 = Random.int (n + 1) in
  let n2 = n - n1 in
  assert (0 <= n1 && n1 <= n);
  assert (0 <= n2 && n2 <= n);
  n1, n2

(* Randomly generate a name between "a" and "z". *)

let random_name () =
  let i = Random.int 26 in
  let c = Char.chr (Char.code 'a' + i) in
  String.make 1 c

(* Randomly generate a raw term whose free names are in the set [env]
   and whose size is exactly [n]. *)

let rec generate_raw env n =
  assert (n >= 0);
  assert (StringSet.cardinal env > 0);
  if n = 0 then
    TVar (StringSet.pick env)
  else
    match Random.int 2 with
    | 0 ->
        let n1, n2 = split (n - 1) in
        TApp (generate_raw env n1, generate_raw env n2)
    | 1 ->
        generate_raw_lambda env n
    | _ ->
        assert false

and generate_raw_lambda env n =
  assert (n > 0);
  let n = n - 1 in
  let x = random_name () in
  let env = StringSet.add x env in
  TLambda (x, generate_raw env n)

(* Randomly generate a closed raw term of size [n]. *)

let generate_raw n =
  assert (n > 0);
  (* Begin with [TLambda], so we have a nonempty [env]. *)
  generate_raw_lambda StringSet.empty n

(* Randomly generate a nominal term whose bound and free names are in the set [env]
   and whose size is [n]. *)

let rec generate_nominal env n =
  assert (n >= 0);
  assert (AtomSet.cardinal env > 0);
  if n = 0 then
    TVar (AtomSet.pick env)
  else
    match Random.int 2 with
    | 0 ->
        let n1, n2 = split (n - 1) in
        TApp (generate_nominal env n1, generate_nominal env n2)
    | 1 ->
        generate_nominal_lambda env n
    | _ ->
        assert false

and generate_nominal_lambda env n =
  assert (n > 0);
  let n = n - 1 in
  let x = AtomSet.pick env in
  TLambda (x, generate_nominal env n)

(* Generate a set of [k] fresh atoms. *)

let rec atoms accu k =
  if k = 0 then
    accu
  else
    let a = AlphaLib.Atom.freshh "a" in
    atoms (AtomSet.add a accu) (k - 1)

let atoms =
  atoms AtomSet.empty

(* Randomly generate a nominal term whose bound and free names are taken from a set
   of [k] fresh atoms and whose size is [n]. Note that this term does not satisfy
   the GUH. *)

let generate_nominal k n =
  generate_nominal (atoms k) n
